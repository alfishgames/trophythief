﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camera : MonoBehaviour
{
    public enum State { Start, Gameplay }
    public State state;
    private Transform target;
    [Range(0.0f,10.0f)]
    public float smoothness;
    public Vector3 offset, offset2;

    private void Awake()
    {
        target = GameObject.FindGameObjectWithTag("Player").transform;
    }

    private void Update()
    {
        if (Movement.instance.movementState == Movement.MovementState.Sprinting) state = State.Gameplay;
    }

    private void FixedUpdate()
    {
        GamePlay();
    }

    public void GamePlay()
    {
        if(state == State.Start)
        {
            Vector3 desiredPosition = target.position + offset2;
            transform.position = Vector3.Lerp(transform.position, desiredPosition, Time.deltaTime * smoothness);

            smoothness = Mathf.Lerp(smoothness, 3, Time.deltaTime);
        }

        if(state == State.Gameplay)
        {
            Vector3 desiredPosition = target.position + offset;
            Quaternion desiredRotation = Quaternion.Euler(10, 0, 0);

            smoothness = Mathf.Lerp(smoothness, 3, Time.deltaTime);
            transform.rotation = Quaternion.Lerp(transform.rotation, desiredRotation, Time.deltaTime);
            transform.position = Vector3.Lerp(transform.position, desiredPosition, Time.deltaTime * smoothness);
        }
    }
}
