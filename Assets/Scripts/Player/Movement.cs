﻿using RootMotion.FinalIK;
using UnityEngine;
using UnityEngine.UI;

public class Movement : MonoBehaviour
{
    public static Movement instance;
    public enum MovementState { Idle, CollectTrophy, Sprinting, Running, Dead }
    public MovementState movementState;
    public Slider staminaBar;
    public GameObject trophy;
    public Transform hand;
    public float hSpeed, runSpeed, sprintSpeed, stamina, maxStamina;
    public bool collectTrophy;
    public Vector3 offset;
    private GameObject standTropy;

    //[HideInInspector]
    public int levelGold, levelTurbo, levelStamina, gold, distance, earnedGold;

    //[HideInInspector]
    public int upgradeTurboCost, upgradeGoldCost, upgradeStaminaCost;

    [HideInInspector]
    public Rigidbody myRb;

    public float animTime , animTimer;

    private SphereCollider barrierCol;
    private float horizontalMove, mouseX;
    public bool animFwd;
    private Animator anim;
    private Vector3 hMovement, vMovement, mMovement, startPoint;
    private Text meter, goldText;
    private FullBodyBipedIK ik;
    private string goldPref, turboPref, goldUpPref, staminaPref, turboCostPref, goldCostPref, staminaCostPref , turboMaxStaminaPref, sprintSpeedPref;
    private Text upgradeTurboText, upgradeGoldText, upgradeStaminaText, levelGoldText, levelTurboText, levelStaminaText;

    private void Awake()
    {
        if (instance == null) instance = this;
        
        #region GetComponent
        myRb = GetComponent<Rigidbody>();
        movementState = MovementState.Idle;
        anim = GetComponent<Animator>();
        meter = GameObject.Find("Meter Text").GetComponent<Text>();
        goldText = GameObject.Find("Cash Text").GetComponent<Text>();
        ik = GetComponent<FullBodyBipedIK>();
        barrierCol = GameObject.Find("Barrier Collider").GetComponent<SphereCollider>();
        standTropy = GameObject.Find("kupa_stand");
        upgradeTurboText = GameObject.Find("Upgrade Turbo Button Text").GetComponent<Text>();
        upgradeGoldText = GameObject.Find("Upgrade Gold Button Text").GetComponent<Text>();
        upgradeStaminaText = GameObject.Find("Upgrade Stamina Button Text").GetComponent<Text>();
        levelTurboText = GameObject.Find("Turbo Level Text").GetComponent<Text>();
        levelGoldText = GameObject.Find("Gold Level Text").GetComponent<Text>();
        levelStaminaText = GameObject.Find("Stamina Level Text").GetComponent<Text>();
        #endregion

        #region Pref Strings

        goldPref = "gold";
        turboPref = "turbo";
        goldUpPref = "goldUp";
        staminaPref = "stamina";
        turboCostPref = "turboCost";
        goldCostPref = "goldUpCost";
        staminaCostPref = "staminaCost";
        sprintSpeedPref = "sprintSpeed";
        turboMaxStaminaPref = "maxStamina";

        #endregion

        startPoint = transform.position;

        #region Get-Set Prefs

        gold = PlayerPrefs.GetInt(goldPref);
        levelGold = PlayerPrefs.GetInt(goldUpPref);
        levelTurbo = PlayerPrefs.GetInt(turboPref);
        levelStamina = PlayerPrefs.GetInt(staminaPref);

        if (!PlayerPrefs.HasKey(turboCostPref)) PlayerPrefs.SetInt(turboCostPref, 40);
        if (!PlayerPrefs.HasKey(goldCostPref)) PlayerPrefs.SetInt(goldCostPref, 40);
        if (!PlayerPrefs.HasKey(staminaCostPref)) PlayerPrefs.SetInt(staminaCostPref, 40);
        if (!PlayerPrefs.HasKey(turboMaxStaminaPref)) PlayerPrefs.SetFloat(turboMaxStaminaPref, 5);
        if (!PlayerPrefs.HasKey(sprintSpeedPref)) PlayerPrefs.SetFloat(sprintSpeedPref, 50);

        upgradeTurboCost = PlayerPrefs.GetInt(turboCostPref);
        upgradeGoldCost = PlayerPrefs.GetInt(goldCostPref);
        upgradeStaminaCost = PlayerPrefs.GetInt(staminaCostPref);
        maxStamina = PlayerPrefs.GetFloat(turboMaxStaminaPref);
        sprintSpeed = PlayerPrefs.GetFloat(sprintSpeedPref);

        #endregion

        #region Awake UI Values

        goldText.text = gold.ToString();
        upgradeTurboText.text = upgradeTurboCost.ToString();
        upgradeGoldText.text = upgradeGoldCost.ToString();
        upgradeStaminaText.text = upgradeStaminaCost.ToString();
        if (levelTurbo == 0) levelTurboText.text = "Not Upgraded";
        else levelTurboText.text = "Level " + levelTurbo.ToString();
        if (levelGold == 0) levelGoldText.text = "Not Upgraded";
        else levelGoldText.text = "Level " + levelGold.ToString();
        if (levelStamina == 0) levelStaminaText.text = "Not Upgraded";
        else levelStaminaText.text = "Level " + levelStamina.ToString();

        #endregion

        stamina = maxStamina;
        staminaBar.maxValue = runSpeed;
    }

    private void Update()
    {
        DeclaringInputs();
        Idle();
        CollectingTrophy();
        Sprinting();
        Running();
        Dead();
        FallFwd();
        ManageUI();
    }

    private void DeclaringInputs()
    {
        if (Input.GetMouseButton(0) && MyGameManager.instance.startGame && movementState != MovementState.CollectTrophy && movementState != MovementState.Dead)
        {

            mouseX = Input.GetAxis("Mouse X") * 10f;
            Quaternion playerRotation = Quaternion.Euler(0, 20 * mouseX, 0);
            horizontalMove = mouseX;
            transform.rotation = Quaternion.Lerp(transform.rotation, playerRotation, Time.deltaTime * 10);
            hMovement = new Vector3(horizontalMove, 0, 0) * hSpeed;
        }

        else
        {
            transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.identity, Time.deltaTime * 10);
        }

        if (Input.GetMouseButtonUp(0) && MyGameManager.instance.startGame)
        {
            hMovement = Vector3.zero;
            mouseX = 0;
        }

        
        vMovement = Vector3.forward;
        transform.position = new Vector3(Mathf.Clamp(transform.position.x, -17.15f, 17.15f), transform.position.y, transform.position.z);
    }

    private void Idle()
    {
        if (movementState == MovementState.Idle)
        {
            if (MyGameManager.instance.startGame)
            {
                movementState = MovementState.CollectTrophy;
            }
        }
    }

    private void CollectingTrophy()
    {
        if (movementState == MovementState.CollectTrophy)
        {
            anim.SetBool("SneakyRun", true);
            myRb.velocity = new Vector3(trophy.transform.position.x - transform.position.x, 0, trophy.transform.position.z - transform.position.z).normalized * 10;
            transform.LookAt(new Vector3(trophy.transform.position.x, 0, trophy.transform.position.z));
            if (collectTrophy)
            {
                trophy.transform.parent = GameObject.FindGameObjectWithTag("Player").transform;
                trophy.transform.position = hand.position + offset;

                movementState = MovementState.Sprinting;

            }
        }
    }

    private void Sprinting()
    {
        if (movementState == MovementState.Sprinting && stamina > 0)
        {
            anim.SetBool("Sprint", true);
            stamina -= Time.deltaTime;
            myRb.velocity = vMovement * sprintSpeed + hMovement * 0.6f;

            transform.position += hMovement * Time.deltaTime * 0.4f;

            ik.solver.IKPositionWeight = Mathf.Lerp(ik.solver.IKPositionWeight, 1, Time.deltaTime * 3);
            trophy.transform.rotation = Quaternion.Euler(0, 0, 0);

            if (stamina < 0)
            {
                stamina = 0;
                movementState = MovementState.Running;
                SetAnimsFalse();
            }
        }
    }

    private void Running()
    {
        if (movementState == MovementState.Running)
        {
            anim.SetBool("Run", true);
            myRb.velocity = vMovement * runSpeed + hMovement *  0.6f;

            transform.position += hMovement * Time.deltaTime * 0.4f;

            barrierCol.gameObject.transform.GetChild(0).gameObject.SetActive(false);
            barrierCol.enabled = false;


            float increaser = Mathf.Pow(1 + levelStamina, 0.0018f) - 1;

            runSpeed -= Time.deltaTime - increaser;

            if (runSpeed <= 10)
            {
                runSpeed = 10;
                EnemyAI.instance.state = EnemyAI.State.Chasing;
                staminaBar.gameObject.SetActive(false);
            }
        }
    }

    private void Dead()
    {
        if (movementState == MovementState.Dead)
        {
            MyGameManager.instance.Fail();
        }
    }

    private void ManageUI()  // FUNCTION // ** UPDATE() UI VARIABLES. **
    {
 
        staminaBar.value = runSpeed;
        staminaBar.minValue = 10;
        float meterCalculate = transform.position.z - startPoint.z;
        meter.text = (meterCalculate * 0.5f).ToString("F0") + " m";
        goldText.text = gold.ToString();
        distance = Mathf.RoundToInt(meterCalculate * 0.5f);
    }

    [ContextMenu("Reset Prefs")]
    public void DeletePlayerPrefs()
    {
        PlayerPrefs.DeleteAll();
    }

    public void UpgradeTurbo()  // FUNCTION // ** INCREASES TURBO. PLAYER'S SPRINT LASTS LONGER AND SPRINT SPEED INCREASES. **
    {
        if (gold >= upgradeTurboCost)
        {
            gold -= upgradeTurboCost;

            levelTurbo++;
            upgradeTurboCost = Mathf.RoundToInt(upgradeTurboCost * Mathf.Pow(levelTurbo + 1, 0.1f) * 1.13f);
            maxStamina += 0.1f;
            sprintSpeed += 0.25f;
            stamina = maxStamina;

            goldText.text = gold.ToString();
            upgradeTurboText.text = upgradeTurboCost.ToString();
            levelTurboText.text = "Level " + levelTurbo.ToString();

            PlayerPrefs.SetInt(goldPref, gold);
            PlayerPrefs.SetInt(turboPref, levelTurbo);
            PlayerPrefs.SetInt(turboCostPref, upgradeTurboCost);
            PlayerPrefs.SetFloat(turboMaxStaminaPref, maxStamina);
            PlayerPrefs.SetFloat(sprintSpeedPref, sprintSpeed);
        }
    }

    public void UpgradeGold()  //FUNCTION // ** INCREASES PLAYER'S INCOME GOLD. **
    {
        if (gold >= upgradeGoldCost)
        {
            gold -= upgradeGoldCost;

            levelGold++;
            upgradeGoldCost = Mathf.RoundToInt(upgradeGoldCost * Mathf.Pow(levelGold + 1, 0.1f) * 1.2f);

            goldText.text = gold.ToString();
            upgradeGoldText.text = upgradeGoldCost.ToString();
            levelGoldText.text = "Level " + levelGold.ToString();

            PlayerPrefs.SetInt(goldPref, gold);
            PlayerPrefs.SetInt(goldUpPref, levelGold);
            PlayerPrefs.SetInt(goldCostPref, upgradeGoldCost);
        }
    }

    public void UpgradeStamina()  // FUNCTION // ** INCREASES STAMINA. PLAYER EXHAUSTS LATE. **
    {
        if (gold >= upgradeStaminaCost) 
        {
            gold -= upgradeStaminaCost;

            levelStamina++;
            upgradeStaminaCost = Mathf.RoundToInt(upgradeStaminaCost * Mathf.Pow(levelStamina + 1, 0.1f) * 1.15f);

            goldText.text = gold.ToString();
            upgradeStaminaText.text = upgradeStaminaCost.ToString();
            levelStaminaText.text = "Level " + levelStamina.ToString();

            PlayerPrefs.SetInt(goldPref, gold);
            PlayerPrefs.SetInt(staminaPref, levelStamina);
            PlayerPrefs.SetInt(staminaCostPref, upgradeStaminaCost);
        }
    }

    public void FallFwd()
    {
        if (animFwd)
        {
            animTimer += Time.deltaTime;
            if (animTimer >= animTime)
            {
                anim.SetBool("FallFwd", true);
            }
        }   
    }

    private void SetAnimsFalse()
    {
        anim.SetBool("SneakyRun", false);
        anim.SetBool("FallDown", false);
        anim.SetBool("PickUp", false);
        anim.SetBool("Sprint", false);
        anim.SetBool("Run", false);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Trophy"))
        {
            if (!collectTrophy)
            {
                standTropy.AddComponent<BoxCollider>();
                standTropy.AddComponent<Rigidbody>().AddForce(Vector3.left * 4, ForceMode.Impulse);
            }

            collectTrophy = true;
        }

        if (other.gameObject.CompareTag("Slower") && movementState == MovementState.Running)
        {
            runSpeed -= 1;
        }

        if (other.gameObject.CompareTag("Cone") && movementState == MovementState.Running)
        {

            Rigidbody rb = other.gameObject.GetComponent<Rigidbody>();
            rb.useGravity = true;
            Vector3 forceDir = other.gameObject.transform.position - transform.position;
            rb.AddForce((forceDir + Vector3.right * 3 * forceDir.x) * 4, ForceMode.Impulse);
            rb.AddTorque(-forceDir * 2, ForceMode.Impulse);
            runSpeed -= 1f;
        }

        if ((other.gameObject.CompareTag("Enemy") || other.gameObject.CompareTag("Killer")) && movementState == MovementState.Running)
        {
            
            if (other.gameObject.CompareTag("Killer"))
            {
                anim.SetBool("FallBack", true);
                myRb.velocity = Vector3.zero;
            }
            else if (other.gameObject.CompareTag("Enemy")) animFwd = true;
            trophy.transform.parent = null;
            trophy.gameObject.GetComponent<Rigidbody>().useGravity = true;
            trophy.gameObject.GetComponent<Rigidbody>().isKinematic = false;
            trophy.gameObject.GetComponent<BoxCollider>().isTrigger = false;
            trophy.gameObject.GetComponent<Rigidbody>().AddTorque(Vector3.left, ForceMode.Impulse);
            ik.enabled = false;
            earnedGold = Mathf.RoundToInt((distance * 0.115f) * Mathf.Pow(levelGold + 1, 0.3f));
            gold += earnedGold;
            goldText.text = gold.ToString();
            PlayerPrefs.SetInt(goldPref, gold);
            movementState = MovementState.Dead;
        }


        if ((other.gameObject.CompareTag("Killer") || other.gameObject.CompareTag("Cone")) && movementState == MovementState.Sprinting)
        {
            Rigidbody rb = other.gameObject.GetComponent<Rigidbody>();
            Vector3 forceDir = other.gameObject.transform.position - transform.position;
            rb.useGravity = true;
            other.gameObject.GetComponent<Collider>().isTrigger = false;
            //rb.AddForce(forceDir * 5, ForceMode.Impulse);
            //rb.AddTorque(-forceDir * 2, ForceMode.Impulse);
            rb.AddExplosionForce(250f, transform.position + Vector3.down * 5f, 15f);

        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (((other.gameObject.CompareTag("Slower") || other.gameObject.CompareTag("Cone")) && movementState == MovementState.Running) && runSpeed > 10.1f)
        {
          //  runSpeed += 7;
            other.gameObject.GetComponent<BoxCollider>().isTrigger = false;
        }
    }
}
