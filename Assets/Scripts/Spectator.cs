﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spectator : MonoBehaviour
{
    private Animator anim;
    public int animNumber;
    public string numberString;

    private float timer;

    private void Awake()
    {
        anim = GetComponent<Animator>();
        animNumber = Random.Range(1, 7);
        numberString = animNumber.ToString();
        anim.SetTrigger(numberString);
    }

    private void Update()
    {
        timer += Time.deltaTime;

        if (timer >= 1)
        {
            timer = 0;
            animNumber = Random.Range(1, 7);
            numberString = animNumber.ToString();
            anim.SetTrigger(numberString);
        }
    }
}
