﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAI : MonoBehaviour
{
    public static EnemyAI instance;
    public enum State { Idle, Running, Chasing, Hit }
    public State state;
    public float mySpeed;

    private Animator anim;
    private Rigidbody myRb;
    private GameObject player;
    private Vector3 target;
    private bool k;

    private void Awake()
    {
        if (instance == null) instance = this;
        myRb = GetComponent<Rigidbody>();
        anim = GetComponent<Animator>();
        player = GameObject.FindGameObjectWithTag("Player");
    }

    private void Update()
    {
        CalculateVariables();
        Idle();
        Running();
        ChasePlayer();
        Hit();
    }

    private void Idle()
    {
        if(state == State.Idle)
        {
            myRb.velocity = Vector3.zero;
            if(Movement.instance.movementState == Movement.MovementState.Sprinting)
            {
                state = State.Running;
            }
        }
    }

    private void Running()
    {
        if(state == State.Running)
        {
            anim.SetBool("Enemy Run", true);
            myRb.velocity = (target.normalized) * mySpeed;

            if (Movement.instance.movementState == Movement.MovementState.Dead)
            {
                state = State.Idle;
            }
        }
    }

    private void ChasePlayer()
    {
        if (state == State.Chasing)
        {
            myRb.velocity = target.normalized * mySpeed * 2f;
        }
    }

    private void Hit()
    {
        if (state == State.Hit)
        {
            anim.SetBool("Enemy Run", false);
            anim.SetBool("Hit", true);
            myRb.velocity = Vector3.zero;
            Movement.instance.myRb.velocity = Vector3.zero;
        }
    }

    private void CalculateVariables()
    {
        //Calculate target and distance from player
        target = player.transform.position - transform.position;
        transform.LookAt(player.transform);

        //Calculate mySpeed
        if (MyGameManager.instance.startGame)
        {
            mySpeed = Mathf.Lerp(0, Movement.instance.sprintSpeed, Time.deltaTime * 80);
        }

        if(Movement.instance.movementState == Movement.MovementState.Running)
        {
            if(target.z >= 8 && !k)
            {                
                mySpeed = Movement.instance.runSpeed * 1.5f;
            }
            else
            {
                k = true;
                mySpeed = Movement.instance.runSpeed;
            }   
        }
    }
        

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("Player"))
        {
            state = State.Hit;
        }
    }
}
