﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PowerBar : MonoBehaviour
{
    public GameObject background, indicator;
    public enum State { Moving, Selected }
    public State state;
    public float speed;
    public string color;
    public Text perfectText;

    private Vector3 startPos;

    private void Awake()
    {
        startPos = indicator.transform.position;
    }

    private void Update()
    {
        Moving();
        ColorPicker();
        SelectedColor();
    }

    private void Moving()
    {
        if(state == State.Moving)
        {
            indicator.transform.position = new Vector3(startPos.x / 2 + 50 + Mathf.PingPong(Time.time * speed * 50, 440), transform.position.y, indicator.transform.position.z);
        }    
    }

    private void ColorPicker()
    {
        if(state == State.Moving)
        {
            float a = indicator.transform.position.x - 320;
            if (a < 50) color = "red";
            else if (a > 50 && a < 122) color = "orange";
            else if (a > 122 && a < 190) color = "yellow";
            else if (a > 190 && a < 246) color = "green";
            else if (a > 246 && a < 313) color = "yellow";
            else if (a > 313 && a < 381) color = "orange";
            else if (a > 381 && a < 439) color = "red";
        }
        

        if (MyGameManager.instance.startGame)
        {
            state = State.Selected;
        }
    }

    private void SelectedColor()
    {
        if(state == State.Selected)
        {
            if (color == "red")
            {
                Movement.instance.sprintSpeed *= 0.7f;
                Movement.instance.runSpeed *= 0.7f;
                Movement.instance.maxStamina *= 0.7f;
                Movement.instance.stamina *= 0.7f;
            }

            if (color == "orange")
            {
                Movement.instance.sprintSpeed *= 0.8f;
                Movement.instance.runSpeed *= 0.8f;
                Movement.instance.maxStamina *= 0.8f;
                Movement.instance.stamina *= 0.8f;
            }

            if (color == "yellow")
            {
                Movement.instance.sprintSpeed *= 0.9f;
                Movement.instance.runSpeed *= 0.9f;
                Movement.instance.maxStamina *= 0.9f;
                Movement.instance.stamina *= 0.9f;
            }

            if (color == "green")
            {
                Movement.instance.sprintSpeed *= 1;
                Movement.instance.runSpeed *= 1;
                Movement.instance.maxStamina *= 1;
                Movement.instance.stamina *= 1;
            }

            StartCoroutine(ManageUI());
        }
    }

    IEnumerator ManageUI()
    {
        if (color == "green")
        {
            perfectText.gameObject.SetActive(true);
            color = null;
        }
        else color = null;
       
        yield return new WaitForSeconds(0.3f);
        indicator.SetActive(false);
        background.SetActive(false);
        yield return new WaitForSeconds(0.5f);
        perfectText.gameObject.SetActive(false);
    }
}
