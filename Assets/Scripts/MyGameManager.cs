﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MyGameManager : MonoBehaviour
{
    public static MyGameManager instance;

    public Text earnedGoldText, distanceText;

    [HideInInspector]
    public bool startGame, failed, successed;
    public GameObject successPanel, failPanel, startGamePanel;
    public float earnedGold, distance, newDistance;
    private string distancePref;

    private void Awake()
    {
        if (instance == null) instance = this;
        distancePref = "distancePref";
        if (!PlayerPrefs.HasKey(distancePref)) PlayerPrefs.SetFloat(distancePref, 0);
        distance = PlayerPrefs.GetFloat(distancePref, distance);
    }

    private void Update()
    {
        CheckEndGameState();
        if (Input.GetMouseButtonDown(0) && !startGame) StartGame();
    }

    public void StartGame()
    {
        startGame = true;
        startGamePanel.SetActive(false);
    }

    public void CheckEndGameState()
    {
        if (!failed) newDistance = Movement.instance.distance;
        if (failed) 
        {
            earnedGold += Time.deltaTime * 15;

            if(earnedGold >= Movement.instance.earnedGold)
            {
                earnedGold = Movement.instance.earnedGold;
            }
        }

        if (failed) earnedGoldText.gameObject.SetActive(true);
        if (failed) earnedGoldText.text = "+" + earnedGold.ToString("F0");
        if (failed && (distance < newDistance))
        {
            distanceText.gameObject.SetActive(true);
            distanceText.text = "New Record : " + newDistance.ToString() + " m";
            distance = newDistance;
            PlayerPrefs.SetFloat(distancePref, distance);
        }
    }

    public void Fail()
    {
        failed = true;
        failPanel.SetActive(true);      
    }

    public void RestartGame()
    {
        LoadAsync.GlobalAsync.TriggerManually();
        Destroy(this);
    }
    

}
