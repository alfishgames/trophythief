﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoadManager : MonoBehaviour
{
    public static RoadManager instance;
    public GameObject[] roadPrefabs;
    public GameObject stadium;
    public List<GameObject> roadList = new List<GameObject>();

    private float timer;
    private int deleteRoad;
    public Vector3 k; // set stadium distance to this.
    public Vector3 l; // set road distance to this.

    private void Awake()
    {
        if (instance == null) instance = this;
        roadList.Add(Instantiate(roadPrefabs[Random.Range(0, roadPrefabs.Length - 1)], stadium.transform.position + k, Quaternion.Euler(0,180,0)));
        roadList.Add(Instantiate(roadPrefabs[Random.Range(0, roadPrefabs.Length - 1)], roadList[roadList.Count - 1].transform.position + l, Quaternion.Euler(0, 180, 0)));
        roadList.Add(Instantiate(roadPrefabs[Random.Range(0, roadPrefabs.Length - 1)], roadList[roadList.Count - 1].transform.position + l, Quaternion.Euler(0, 180, 0)));
    }

    private void Update()
    {
        ManageRoads();
    }

    private void ManageRoads()
    {
        timer += Time.deltaTime;

        if(timer >= 4f)
        {
            roadList.Add(Instantiate(roadPrefabs[Random.Range(0, roadPrefabs.Length - 1)], roadList[roadList.Count - 1].transform.position + l, Quaternion.Euler(0, 180, 0)));
            timer = 0f;
            deleteRoad++;
        }

        //if(deleteRoad >= 4)
        //{
        //    Destroy(roadList[0]);
        //    roadList.RemoveAt(0);
        //    Destroy(roadList[0]);
        //    roadList.RemoveAt(0);
        //    deleteRoad = 0;
        //}
    }
}
